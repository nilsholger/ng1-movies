'use strict';

angular.module('ng1Movies')
  .controller('MoviesCtrl', function ($scope, $location, $http) {

    $http.get('/movies.json').then(function(response){
          for (var i in response.data) {
            if (response.data !== undefined){
            response.data[i].slug = response.data[i].title.toLowerCase().replace(/ /g, '-');
          }
        }
          $scope.movies = response.data;
    });

    $scope.movie = {
      title: '',
      description: '',
      category: '',
      image: 'https://digitalprolab.com/products/video/images/Movie-Reel-hero.jpg',
    };

    $scope.goToRandomMovie = function() {
      var index = Math.floor(Math.random() * $scope.movies.length);
      var movie = $scope.movies[index];
      var urlMa = 'movie/' + movie.id + '/' + movie.slug;
      $location.url(urlMa);
    }

    $scope.isValid = function() {
          if ($scope.movie.title === ''){
            return false;
          }
          if ($scope.movie.description === ''){
            return false;
          }
          if ($scope.movie.category === ''){
            return false;
          }
          return true;
    }

    $scope.validateTitle = function(){
      if ($scope.movie.title.length > 0){
        console.debug($scope.movie.title);
      } else {
        window.alert('title is required');
      }
    };

    $scope.addMovie = function() {
      $scope.movies.push(angular.copy($scope.movie));
    };

    $scope.checkCategorySelected = function() {
      if ($scope.movie.category === ''){
        window.alert('category cannot be empty');
      }
    };

    $scope.checkDescription = function() {
      console.debug($scope.newMovieDescription);
    }

  }).controller('SubCtrl', function($scope) {
    $scope.title = 'available to watch: ' + $scope.movies.length + ' movies';
  }).controller('FormCtrl', function($scope) {
    console.log('we have a form controller');
  });

'use strict';
angular.module('ng1Movies').controller('MovieCtrl', function($scope, $routeParams){
var id = parseInt($routeParams.id, 10);

var movieList = [{
  id: 23,
  title : 'Star Wars',
  slug: 'star-wars1',
  image : 'http://ia.media-imdb.com/images/M/MV5BMTU4NTczODkwM15BMl5BanBnXkFtZTcwMzEyMTIyMw@@._V1_SX214_.jpg',
  description : 'This 13 chapter serial is based on the comic strip character Ace Drummond created by Eddie Rickenbacker. Ace is a \'G-Man of the sky\' working out of Washington D.C.'
},
{
  id: 5,
title : 'La guerra de las galaxias (1977)',
slug: 'la-guerra',
image : 'http://ia.media-imdb.com/images/M/MV5BMTU4NTczODkwM15BMl5BanBnXkFtZTcwMzEyMTIyMw@@.jpg',
description: 'this is description 1'
},
{
id: 42,
title : 'Star Wars: Episodio VII - El despertar de la fuerza (2015)',
slug : 'star-wars',
image : 'http://ia.media-imdb.com/images/M/MV5BMTkwNzAwNDA4N15BMl5BanBnXkFtZTgwMTA2MDcwNzE@.jpg',
description: 'this is description 2'
}
];


for (var index in movieList){
  var movie = movieList[index];
  if (movie.id === id){
    $scope.movie = movie;
    break;
  }
}
});
